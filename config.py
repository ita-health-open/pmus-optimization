NSIMULATIONS = 60000
NTRIALS = 50
NCROSSVAL = 3
SCALERFILENAME = 'standard_3d_scaler.pkl'
TARGETS = ['R', 'C']
ATTRIBUTES = ['Flow', 'Volume', 'Paw']
VERBOSE = True
