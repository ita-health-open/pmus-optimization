from scipy.io import loadmat
import utils
import config
import numpy as np
import time
import matplotlib.pyplot as plt
import pickle
from simulator.parameters import Fs, RR

if __name__ == '__main__':

    filename = './data/asl.mat'

    models_filename = ['test']

    annots = loadmat(filename)

    offset = 10

    size = 60000

    def remove_peep(ins_mark, exp_mark, paw):
        ins_indexes = [i for i in range(
            len(~np.isnan(ins_mark))) if ~np.isnan(ins_mark)[i]][:-1]
        exp_indexes = [i for i in range(
            len(~np.isnan(ins_mark))) if ~np.isnan(exp_mark)[i]][1:]
        i = 0
        for ins, exp in zip(ins_indexes, exp_indexes):
            peep = np.average(paw[ins-int(512/5):ins])
            if i == 0:
                paw[:exp] -= peep

            else:
                paw[last_exp:exp] -= peep

            last_ins, last_exp = ins, exp
            i += 1

        return paw

    aux = [elem[0]*60/1000 for index,
           elem in enumerate(annots['flowLmin']) if index % offset == 0]
    flow = []
    for index in range(len(aux)//901):
        flow.append(aux[index*901:(index+1)*901])

    flow = np.array(flow)

    ins_mark = np.array([elem[0] for elem in annots['ins_mark']])
    exp_mark = np.array([elem[0] for elem in annots['exp_mark']])

    _paw = np.array([elem[0] for elem in annots['paw']])
    #_paw = remove_peep(ins_mark, exp_mark, _paw)

    aux = np.array(
        [elem for index, elem in enumerate(_paw) if index % offset == 0])

    paw = []
    for index in range(len(aux)//901):
        paw.append(aux[index*901:(index+1)*901])

    paw = np.array(paw)

    aux = [elem[0] for index, elem in enumerate(
        annots['pmusASL']) if index % offset == 0]
    pmus = []
    for index in range(len(aux)//901):
        pmus.append(aux[index*901:(index+1)*901])
    pmus = np.array(pmus)

    aux = [elem[0] for index, elem in enumerate(
        annots['volint']) if index % offset == 0]
    volume = []
    for index in range(len(aux)//901):
        volume.append(aux[index*901:(index+1)*901])
    volume = np.array(volume)

    num_examples = flow.shape[0]
    num_samples = flow.shape[1]

    scaler = pickle.load(open('./data/'+config.SCALERFILENAME, 'rb'))

    input_data = np.zeros((num_examples, num_samples, 3))
    input_data[:, :, 0] = scaler.fit_transform(flow)
    input_data[:, :, 1] = scaler.fit_transform(volume)
    input_data[:, :, 2] = scaler.fit_transform(paw)

    models = [utils.load_model(f) for f in models_filename]

    output_pred_test = models[0].predict(input_data)
    output_pred_test = scaler.inverse_transform(output_pred_test)

    err_r = []
    err_c = []
    err_pmus = []
    err_pmus_hat = []
    err_nmsre = []

    rr = min(RR)
    fs = max(Fs)
    time = np.arange(0, np.floor(180.0 / rr * fs) + 1, 1) / fs

    C = 40
    R = 12
    for i in range(num_examples-1):

        R_hat = output_pred_test[i, 0]
        C_hat = output_pred_test[i, 1]

        f = flow[i, :]
        v = volume[i, :]
        p = paw[i, :]

        print("R:", R_hat)
        print("C:", C_hat)

        pmus_hat = p - (R_hat) * f * 1000.0 / 60.0 - (1 / C_hat) * v

        fig, (ax1, ax2) = plt.subplots(2)
        ax1.grid()
        ax2.grid()
        ax1.plot(time, pmus[i, :])
        ax1.plot(time, pmus_hat)
        ax1.plot(time, p)
        ax2.plot(time, f/60)

        ax1.legend(['Real Pmus', 'Predicted Pmus', 'Airway Pressure'], bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
                   ncol=3, mode="expand", borderaxespad=0., prop={'size': 10})
        ax1.set_ylabel('Pressure (cmH2O)')
        ax2.set_ylabel('Flow (L/s)')
        ax2.set_xlabel('Time (s)')

        # plt.figure()

        # plt.plot(time,pmus[i,:])
        # plt.plot(time,pmus_hat)
        # # plt.plot(time,volume/10)
        # plt.plot(time,flow/60)
        # plt.plot(time,paw)
        # plt.grid()
        # plt.legend(['Pmus Real (cmH2O)','Pmus Predicted (cmH2O)','Flow (L/s)','Airway Pressure (cmH2O)'])
        # # plt.ylabel('Pressão muscular (cmH2O)')
        # plt.xlabel('Time (s)')
        # # plt.title('Test case %d' % (i + 1))
        plt.savefig('./data/prints/' + 'ppt_pmus_case_test_%d.png' %
                    (i + 1), format='png')
        plt.savefig('./data/prints/' + 'ppt_pmus_case_test_%d.svg' %
                    (i + 1), format='svg')
        plt.savefig('./data/prints/' + 'ppt_pmus_case_test_%d.eps' %
                    (i + 1), format='eps')
        plt.close()

        err_pmus.extend(pmus[i, :])
        err_pmus_hat.extend(pmus_hat)
        err_c.append(C_hat)
        err_r.append(R_hat*1000)
        print('test case %d' % (i+1))
        err_nmsre.append(np.sqrt(np.sum(
            (pmus[i, :] - pmus_hat)**2))/np.sqrt(np.sum((pmus[i, :] - np.average(pmus[i, :]))**2)))
        #print('nmsre :', np.sqrt(np.sum((pmus[i,:] - pmus_hat)**2))/np.sqrt(np.sum((pmus[i,:] - np.average(pmus[i,:]))**2)))

    err_pmus = np.array(err_pmus)
    err_pmus_hat = np.array(err_pmus_hat)
    err_c = np.array(err_c)
    err_r = np.array(err_r)

    nrmse = np.sqrt(np.sum((err_pmus - err_pmus_hat)**2)) / \
        np.sqrt(np.sum((err_pmus - np.average(err_pmus))**2))

    nrmse_c = np.sqrt(np.sum((err_c - C)**2))/C**2
    nrmse_r = np.sqrt(np.sum((err_r - R)**2))/R**2

    with open('./data/results.txt', 'w') as f:
        f.write(f'nrmse : {nrmse}\nnrmse_c : {nrmse_c}\nnrmse_r : {nrmse_r}')
