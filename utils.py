from simulator.generator import Generator
from config import NSIMULATIONS, TARGETS, ATTRIBUTES, VERBOSE
from tensorflow.keras.models import model_from_json
from matplotlib import pyplot as plt

plt.rcParams.update({'font.family': 'serif'})
plt.rcParams.update({'font.size': 14})


def create_dataset(n_simulations: int = NSIMULATIONS, save: bool = False):
    g = Generator()
    g.simulate(n_simulations=n_simulations, verbose=VERBOSE)
    X, Y = g.dataset(targets=TARGETS, attributes=ATTRIBUTES)
    if save:
        g.save_dataset(filename=str(n_simulations))
    return X, Y


def load_dataset(n_simulations: int = NSIMULATIONS):
    g = Generator()
    X, Y = g.load_dataset(str(n_simulations))
    return X, Y


def save_model(filename: str, model) -> None:
    # serialize model to JSON
    model_json = model.model.to_json()
    path = './data/models/'
    with open(path + filename + '.json', 'w') as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.model.save_weights(path + filename + '.h5')


def load_model(filename: str):
    json_file = open('./data/models/' + filename + '.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights('./data/models/' + filename + '.h5')
    return loaded_model


def save_loss(history) -> None:
    path = './data/prints/'
    plt.figure()
    plt.grid()
    plt.plot(history['loss'])
    plt.plot(history['val_loss'])
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Training', 'Validation'], loc='upper left')
    plt.savefig(path+'train_val.eps', format='eps')
    plt.savefig(path+'train_val.png', format='png')
    plt.savefig(path+'train_val.svg', format='svg')
