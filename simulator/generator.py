from numpy.lib.index_tricks import ix_
from .pmus_generator import PmusGenerator
from typing import Dict, List, Tuple
import numpy as np
from . import parameters
from scipy.integrate import odeint
from pathlib import Path

class Generator:

    def __init__(self) -> None:
        super().__init__()
        self.parameters = parameters
        self.pmus_generator = PmusGenerator()
        self.X = None
        self.Y = None
        
    def __reset__(self,n_simulations : int) -> None:
        self.fs = max(self.parameters.Fs)
        self.rr = min(self.parameters.RR)
        self.num_points = int(np.floor(180.0 / self.rr * self.fs)+1)
        self.flow = np.zeros((self.num_points, n_simulations))
        self.volume = np.zeros((self.num_points, n_simulations))
        self.paw = np.zeros((self.num_points, n_simulations))
        self.pmus = np.zeros((self.num_points, n_simulations))
        self.ins = np.zeros((self.num_points, n_simulations))
        self.r = np.zeros((1, n_simulations))
        self.c = np.zeros((1, n_simulations))
        self.X = None
        self.Y = None

    def simulate(self, n_simulations : int, verbose=False) -> None: 
        self.__reset__(n_simulations)
        kp,vp = self.__get_params__(n_simulations)
        kf,vf = self.__get_features__(n_simulations)
        for i in range(n_simulations):
            if i%100==0 and verbose: print(f'Simulating: {i}:{n_simulations}')
            self.flow[:,i], self.volume[:,i], self.paw[:,i], self.pmus[:,i], self.ins[:,i], self.r[0,i],self.c[0,i] = \
            self.__solve_model__(
                header_params  = kp,
                params         = vp[i],
                header_features= kf,
                features = vf[i],
                debugmsg = ''
                )

    def dataset(self, targets : List[str] = [], attributes : List[str] = []) -> Tuple[np.array,np.array]:
        ds = {
            'Flow'       : self.flow, 
            'Volume'     : self.volume, 
            'Paw'        : self.paw, 
            'Pmus'       : self.pmus,
            'R'          : self.r,
            'C'          : self.c,
            'RR'         : self.rr,
            'Ins'        : self.ins,
            'Fs'         : self.fs,
            'NumPoints'  : self.num_points
            }
        
        cols = list(ds.keys())
        ds   = np.array(list(ds.values()),dtype='object')
        ix_attr = [cols.index(col) for col in attributes]
        ix_targ = [cols.index(col) for col in targets]
        self.X, self.Y = ds[ix_attr], ds[ix_targ]
        return self.X,self.Y
    
    def save_dataset(self,filename : str, path : Path = Path().resolve()) -> None:
        np.save(path.joinpath('data/'+filename+'_attributes.npy'),self.X)
        np.save(path.joinpath('data/'+filename+'_targets.npy'),self.Y)

    def load_dataset(self,filename : str, path : Path = Path().resolve()) -> Tuple[np.array,np.array]:
        X = np.load(path.joinpath('data/'+filename+'_attributes.npy'),allow_pickle=True)
        Y = np.load(path.joinpath('data/'+filename+'_targets.npy'),allow_pickle=True)
        return X, Y

    def __get_features__(self,n_simulations):
        headers = list(self.parameters.features.keys())
        samples = []
        for _ in range(n_simulations):
            samples.append( [np.random.choice(arr) for arr in self.parameters.features.values()] )
        
        return headers, samples
        

    def __solve_model__(self,header_params,params,header_features,features,debugmsg):
        fs = params[header_params.index('Fs')]
        rvent = params[header_params.index('Rvent')]
        c = params[header_params.index('C')]
        rins = params[header_params.index('Rins')]
        rexp = rins  # params[4]
        peep = params[header_params.index('PEEP')]
        sp = params[header_params.index('SP')]
        trigger_type = features[header_features.index('Triggertype')]
        trigger_arg = params[header_params.index('Triggerarg')]
        rise_type = features[header_features.index('Risetype')]
        rise_time = params[header_params.index('Risetime')]
        cycle_off = params[header_params.index('Cycleoff')]
        rr = params[header_params.index('RR')]
        pmus_type = features[header_features.index('Pmustype')]
        pp = params[header_params.index('Pp')]
        tp = params[header_params.index('Tp')]
        tf = params[header_params.index('Tf')]
        noise = params[header_params.index('Noise')]

        expected_len = int(np.floor(180.0 / np.min(self.parameters.RR) * np.max(self.parameters.Fs)) + 1)
    
        #Assings pmus profile
        pmus = self.pmus_generator.pmus_profile(fs, rr, pmus_type, pp, tp, tf)
        pmus = pmus + peep #adjusts PEEP
        pmus = np.concatenate((np.array([0]), pmus)) #sets the first value to zero

        
        #Unit conversion from cmH2O.s/L to cmH2O.s/mL
        rins = rins / 1000.0
        rexp = rexp / 1000.0
        rvent = rvent / 1000.0


        #Generates time, flow, volume, insex and paw waveforms
        time = np.arange(0, np.floor(60.0 / rr * fs) + 1, 1) / fs
        time = np.concatenate((np.array([0]), time))
        flow = np.zeros(len(time))
        volume = np.zeros(len(time))
        insex = np.zeros(len(time))
        paw = np.zeros(len(time)) + peep #adjusts PEEP
        len_time = len(time)

        #Peak flow detection
        peak_flow = flow[0]
        detect_peak_flow = False

        #Support detection
        detect_support = False
        time_support = -1

        #Expiration detection
        detect_exp = False
        time_exp = -1

        if trigger_type == 'flow':
            # units conversion from L/min to mL/s
            trigger_arg = trigger_arg / 60.0 * 1000.0

        for i in range(1, len(time)):
            # period until the respiratory effort beginning
            if (((trigger_type == 'flow' and flow[i] < trigger_arg) or
                (trigger_type == 'pressure' and paw[i] > trigger_arg + peep) or
                (trigger_type == 'delay' and time[i] < trigger_arg)) and
                    (not detect_support) and (not detect_exp)):
                paw[i] = peep
                y0 = volume[i - 1]
                tspan = [time[i - 1], time[i]]
                args = (paw[i], pmus[i], c, rins)
                sol = odeint(self.__flow_model__, y0, tspan, args=args)
                volume[i] = sol[-1]
                flow[i] = self.__flow_model__(volume[i], time[i] ,paw[i], pmus[i], c, rins)
                if debugmsg:
                    print('volume[i]= {:.2f}, flow[i]= {:.2f}, paw[i]= {:.2f}, waiting'.format(volume[i], flow[i], paw[i]))

                if (((trigger_type == 'flow' and flow[i] >= trigger_arg) or
                    (trigger_type == 'pressure' and paw[i] <= trigger_arg + peep) or
                    (trigger_type == 'delay' and time[i] >= trigger_arg))):
                    detect_support = True
                    time_support = time[i+1]
                    continue

            # detection of inspiratory effort
            # ventilator starts to support the patient
            elif (detect_support and (not detect_exp)):
                if rise_type == 'step':
                    paw[i] = sp + peep
                elif rise_type == 'exp':
                    rise_type = rise_type if np.random.random() > 0.01 else 'linear'
                    if paw[i] < sp + peep:
                        paw[i] = (1.0 - np.exp(-(time[i] - time_support) / rise_time )) * sp + peep
                    if paw[i] >= sp + peep:
                        paw[i] = sp + peep
                elif rise_type == 'linear':
                    rise_type = rise_type if np.random.random() > 0.01 else 'exp'
                    if paw[i] < sp + peep:
                        paw[i] = (time[i] - time_support) / rise_time * sp + peep
                    if paw[i] >= sp + peep:
                        paw[i] = sp + peep

                y0 = volume[i - 1]
                tspan = [time[i - 1], time[i]]
                args = (paw[i], pmus[i], c, rins)
                sol = odeint(self.__flow_model__, y0, tspan, args=args)
                volume[i] = sol[-1]
                flow[i] = self.__flow_model__(volume[i],time[i],paw[i], pmus[i], c, rins)
                if debugmsg:
                    print('volume[i]= {:.2f}, flow[i]= {:.2f}, paw[i]= {:.2f}, supporting'.format(volume[i], flow[i], paw[i]))

                if flow[i] >= flow[i - 1]:
                    peak_flow = flow[i]
                    detect_peak_flow = False
                elif flow[i] < flow[i - 1]:
                    detect_peak_flow = True

                if (flow[i] <= cycle_off * peak_flow) and detect_peak_flow and i<len_time:
                    detect_exp = True
                    time_exp = i+1    
                    try:
                        paw[i + 1] = paw[i]
                    except IndexError:
                        pass

            elif detect_exp:
                if rise_type == 'step':
                    paw[i] = peep
                elif rise_type == 'exp':
                    if paw[i - 1] > peep:
                        paw[i] = sp * (np.exp(-(time[i] - time[time_exp-1]) / rise_time )) + peep
                    if paw[i - 1] <= peep:
                        paw[i] = peep
                elif rise_type == 'linear':
                    rise_type = rise_type if np.random.random() > 0.01 else 'exp'
                    if paw[i - 1] > peep:
                        paw[i] = sp * (1 - (time[i] - time[time_exp-1]) / rise_time) + peep
                    if paw[i - 1] <= peep:
                        paw[i] = peep

                y0 = volume[i - 1]
                tspan = [time[i - 1], time[i]]
                args = (paw[i], pmus[i], c, rexp + rvent)
                sol = odeint(self.__flow_model__, y0, tspan, args=args)
                volume[i] = sol[-1]
                flow[i] = self.__flow_model__(volume[i],time[i], paw[i], pmus[i], c, rexp + rvent)
                if debugmsg:
                    print('volume[i]= {:.2f}, flow[i]= {:.2f}, paw[i]= {:.2f}, exhaling'.format(volume[i], flow[i], paw[i]))

        #Generates InsEx trace
        if time_exp > -1:
            insex = np.concatenate((np.ones(time_exp), np.zeros(len(time) - time_exp)))

        #Drops the first element
        flow = flow[1:] / 1000.0 * 60.0  # converts back to L/min
        volume = volume[1:]
        paw = paw[1:]
        pmus = pmus[1:] - peep #reajust peep again
        insex = insex[1:]

        flow,volume,pmus,insex,paw = self.__generate_cycle__(expected_len,flow,volume,pmus,insex,paw,peep=peep)        
        flow,volume,paw,pmus,insex = self.__generate_noise__(noise,flow,volume,paw,pmus,insex)

        return flow, volume, paw, pmus, insex, rins, c


    def __generate_cycle__(self, length,*args,peep=None):
        
        startpos  = np.random.randint(0,len(args[0])) 
        delta     = (length - startpos)
        nzeroes   = delta//len(args[0])+1
        
        zpos      = np.random.randint(2,delta//10,size=nzeroes)
        
        res       = [None]*len(args)

        vrandom   = [np.random.uniform(0.95,1.05) if np.random.random() > 0.01 else np.random.uniform(0.1,0.2) for i in range(nzeroes+1)]

        for i in range(len(args)):
            rarr = args[i][-startpos:]
            mode = peep if (not peep is None) and (i==len(args)-1) else 0
            for j in range(nzeroes):
                aux = vrandom[j+1]*args[i] + (1-vrandom[j+1])*mode
                rarr = np.concatenate((rarr,np.ones(zpos[j])*mode,aux))

            res[i] = rarr[:length]
        
        return res

    def __generate_noise__(self,noise,*args):
        
        res = [None]*len(args)
        
        for i in range(len(args)):
            res[i] = args[i] + np.average(args[i])/3*np.random.randn(len(args[i]))*np.sqrt(noise)
        
        return res


    def __flow_model__(self, vol : np.array,t: np.array , paw : np.array, pmus : np.array, c : float, r : float) -> np.array: 
        return (paw - pmus - 1.0 / c * vol) / r

    def __get_params__(self,n_simulations : int) -> Tuple[List[str], List[List[float]]]:
        samples = []
        len_params = len(self.parameters.params)
        list_params = list(self.parameters.params.keys())
        
        while len(samples) < n_simulations:
            
            vec = [None]*len_params
            
            for param, arr in self.parameters.params.items():
                vec[list_params.index(param)] = np.random.choice(arr) 
            
            if self.__is_valid__(vec,list_params):
                samples.append(vec)  
        
        return list_params, samples

    def __is_valid__(self,arr : List[float], map_params : List[str]) -> bool:
        resp = False
        if self.__is_in_range__(arr[map_params.index(self.parameters.RESISTANCE_INSP)],4.0,10.01) and self.__is_in_range__(arr[map_params.index(self.parameters.RESISTANCE_EXP)],4.0,10.01):
            resp = self.__is_in_range__(arr[map_params.index(self.parameters.COMPLIANCE)],60.01,80.01)
        elif self.__is_in_range__(arr[map_params.index(self.parameters.RESISTANCE_INSP)],10.01,20.01) and self.__is_in_range__(arr[map_params.index(self.parameters.RESISTANCE_EXP)],10.01,20.01):
            resp = self.__is_in_range__(arr[map_params.index(self.parameters.COMPLIANCE)],40.01,60.01)
        elif self.__is_in_range__(arr[map_params.index(self.parameters.RESISTANCE_INSP)],20.01,30.01) and self.__is_in_range__(arr[map_params.index(self.parameters.RESISTANCE_EXP)],20.01,30.01):
            resp = self.__is_in_range__(arr[map_params.index(self.parameters.COMPLIANCE)],30.01,40.01)        
        return resp
    
    def __is_in_range__(self, value : float, minimum : float, maximum : float) -> bool:
        return (value >= minimum) and (value <= maximum)


