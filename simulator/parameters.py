from typing import Dict, Generic, Any
import numpy as np
from dataclasses import dataclass

RESISTANCE_INSP: str = 'Rins'
RESISTANCE_EXP: str = 'Rexp'
COMPLIANCE: str = 'C'

Fs = [50.0]  # sample frequency, Hertz (Hz)
Noise = [0.0, 1e-4, 1e-3, 1e-6]  # noise variance over pressure/flow waveforms

Rvent = [0]  # ventilator expiratory valve resistance, cmH2O/(L/s)
Model = ['FOLM']  # respiratory system model
C = np.arange(30.0, 80.01, 0.001)  # respiratory system compliance, (mL/cmH2O)
E2 = [-30, -20, -10, 0, 10, 20, 30]
# [7]     #respiratory system inspiratory resistance, cmH2O/(L/s)
Rins = np.arange(4.0, 30.01, 0.001)
Rexp = Rins  # respiratory system  expiratory resistance, cmH2O/(L/s)

# , 5, 15]      #positive end-expiratory pressure, water-centimeters (cmH2O)
PEEP = [0]
#PEEP = np.arange(0.0, 15.01, 0.001)
SP = np.arange(0.0, 15.01, 0.001)  # support pressure (above PEEP) (cmH2O)
# SP = [5, 10, 15]        #support pressure (above PEEP) (cmH2O)
Triggertype = ['flow']  # ventilator trigger type
Triggerflow = [2]  # airflow, (L/min)
Triggerpressure = [-0.5, -1, -2]  # pressure (cmH2O)
Triggerdelay = np.arange(0.05, 0.101, 0.01)  # delay time (s)
Triggerarg = Triggerflow
# turns off the support, after flow fall below x% of the peak flow
Cycleoff = np.arange(0.10, 0.4, 0.01)
# pressure waveform rises in exponential or linear fashions
Risetype = ['exp', 'linear']
# time (s) to pressure waveform rises from PEEP to SP
Risetime = np.arange(0.15, 0.3, 0.01)

# [10, 15, 25, 35]   #respiratory rate, respirations per minute (rpm)
RR = np.arange(10.0, 35.01, 0.1)
# morphology of the respiratory effort
Pmustype = ['ingmar', 'linear', 'parexp']
# [-5]#, -10, -12]    #Pmus negative peak amplitude (cmH2O)
Pp = np.arange(-15.0, -4.9, 0.001)
Tp = np.arange(0.3, 0.8, 0.001)  # [0.45]#, 0.5]   #Pmus negative peak time (s)
Tf = np.arange(1.0, 1.5, 0.001)  # [0.6]     #Pmus finish time (s)

cycles_repetition = [1]  # repeats n-times the parameter combination

features = {
    'Pmustype': Pmustype,
    'Risetype': Risetype,
    'Triggertype': Triggertype,
    'Model': Model
}

params = {
    'Fs': Fs,
    'Noise': Noise,
    'Rvent': Rvent,
    'C': C,
    'E2': E2,
    'Rins': Rins,
    'Rexp': Rexp,
    'PEEP': PEEP,
    'SP': SP,
    'Triggerflow': Triggerflow,
    'Triggerpressure': Triggerpressure,
    'Triggerdelay': Triggerdelay,
    'Triggerarg': Triggerarg,
    'Cycleoff': Cycleoff,
    'Risetime': Risetime,
    'RR': RR,
    'Pp': Pp,
    'Tp': Tp,
    'Tf': Tf,
    'cycles_repetition': cycles_repetition
}
