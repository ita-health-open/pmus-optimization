import numpy as np
from sklearn.preprocessing import MinMaxScaler
import numpy as np
from typing import Dict, Optional, Tuple


class MinMax(object):

    def __init__(self) -> None:
        super().__init__()
        self.scalerX = None
        self.scalerY = None
        self.X = None
        self.Y = None

    def fit(self, X: np.array, Y: np.array = None) -> None:
        self.scalerX = self.__fit__(X)
        if not Y is None:
            self.scalerY = self.__fit__(Y)

    def transform(self, X: np.array, Y: np.array = None) -> Tuple[np.array, Optional[np.array]]:
        X = self.__transform__(X, self.scalerX)
        if not Y is None:
            Y = self.__transform__(Y, self.scalerY)
            return X, Y
        else:
            return X

    def fit_transform(self, X: np.array, Y: np.array = None) -> Tuple[np.array, Optional[np.array]]:
        self.fit(X, Y)
        return self.transform(X, Y)

    def inverse_transform(self, X: np.array, Y: np.array = None) -> Tuple[np.array, Optional[np.array]]:
        X = self.__inverse_transform__(X, self.scalerX)
        if not Y is None:
            Y = self.__inverse_transform__(Y, self.scalerY)
            return X, Y
        else:
            return X

    def __inverse_transform__(self, X: np.array, scalers: Dict[int, MinMaxScaler]):
        X = X.T
        for ix in range(X.shape[0]):
            X[ix] = scalers[ix].inverse_transform(
                X[ix].reshape(-1, 1)).reshape(X[ix].shape)
        return X.T

    def __transform__(self, X: np.array, scalers: Dict[int, MinMaxScaler]) -> np.array:
        X = X.T
        for ix in range(X.shape[0]):
            X[ix] = scalers[ix].transform(
                X[ix].reshape(-1, 1)).reshape(X[ix].shape)
        return X.T

    def __fit__(self, X: np.array) -> Tuple[Dict[int, MinMaxScaler], np.array]:
        scalers = {}
        X = X.T
        for ix in range(X.shape[0]):
            scalers[ix] = MinMaxScaler()
            scalers[ix].fit(X[ix].reshape(-1, 1))
        return scalers
