import numpy as np
import utils
from scalers.MinMax import MinMax as Scaler
from sklearn.model_selection import cross_val_score, train_test_split
import pickle
from models.cnn1d import CNN1D as Model
import config

params = {
    'filters': 8,
    'kernel_size': 4,
    'n_cnn_layers': 7,
    'alpha_leaky_relu': 5.138511009240617,
    'learning_rate': 0.0005448918179069718,
    'decay': 0.005386838956837386,
    'epochs': 30
}


if __name__ == '__main__':

    ####### Generate data uncommenting the script bellow ############
    X, Y = utils.create_dataset(save=True)
    #X, Y = utils.load_dataset()
    ####### Preprocessing data to get a better shape ################
    X, Y = np.stack([x.T for x in X], axis=2), np.concatenate(
        [y.T for y in Y], axis=1)

    ###### Scaling dataset to use in models #########################
    scaler = Scaler()

    X, Y = scaler.fit_transform(X, Y)
    ###################################################################

    model = Model(
        num_samples=X.shape[1],
        input_volume=X.shape[-1],
        output_size=Y.shape[-1],
        **params
    )

    h = model.fit(X, Y, validation_rate=0.2)
    utils.save_model('test', model)
    utils.save_loss(h.history)
    pickle.dump(scaler, open('./data/'+config.SCALERFILENAME, 'wb'))
