import numpy as np
import utils
from scalers.MinMax import MinMax as Scaler
import optuna
from sklearn.model_selection import cross_val_score, train_test_split
import pickle
from models.cnn1d import CNN1D
import config


def objective(trial: optuna.Trial, X: np.array, Y: np.array):

    params = dict(
        filters=trial.suggest_int('filters', 3, 8),
        kernel_size=trial.suggest_int('kernel_size', 3, 8),
        #pool_size = trial.suggest_int('pool_size',2,4),
        n_cnn_layers=trial.suggest_int('n_cnn_layers', 7, 10),
        alpha_leaky_relu=trial.suggest_uniform(
            'alpha_leaky_relu', 1e-6, 100.0),
        learning_rate=trial.suggest_uniform('learning_rate', 1e-6, 100.0),
        decay=trial.suggest_uniform('decay', 1e-6, 100.0),
        epochs=trial.suggest_int('epochs', 30, 50)
    )

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2)

    model = CNN1D(
        num_samples=X_train.shape[1], input_volume=X_train.shape[-1], output_size=Y_train.shape[-1], **params)

    #score = cross_val_score(model,X,Y,cv=config.NCROSSVAL)

    model.fit(X_train, Y_train)
    Y_hat = model.predict(X_test)
    mse = (Y_hat - Y_test)**2
    mean_mse = np.mean(mse, axis=0) / mse.shape[0]
    accuracy = mean_mse[0]*mean_mse[1]

    return accuracy


if __name__ == '__main__':

    ####### Generate data uncommenting the script bellow ############
    X, Y = utils.create_dataset()

    ####### Preprocessing data to get a better shape ################
    X, Y = np.stack([x.T for x in X], axis=2), np.concatenate(
        [y.T for y in Y], axis=1)

    ###### Scaling dataset to use in models #########################
    scaler = Scaler()

    X, Y = scaler.fit_transform(X, Y)
    ###################################################################

    study = optuna.create_study(direction='minimize')
    study.optimize(lambda trial: objective(
        trial, X, Y), n_trials=config.NTRIALS)
    best_trial = study.best_trial

    with open('./data/output.txt', 'w') as file:
        file.write(
            f'Accuracy: {best_trial.value}\nHyperparameters: {best_trial.params}')

    pickle.dump(scaler, open('./data/'+config.SCALERFILENAME, 'wb'))
