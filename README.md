# pmus-optimization



## Getting started

To make it easy for you, it is just necessary to have docker installed in your machine.

## Run your optimization code in a good computer

```
bash start.sh
```

## Testing a small number of samples

Just change values in config.py

```
NSIMULATIONS = {DESIRED AMOUNT OF SAMPLES}
```
