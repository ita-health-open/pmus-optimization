from typing import Any
from abc import ABC


class ModelInterface(ABC):

    def fit(self, *args, **kwargs) -> None:
        """ Fit models throught data passed in args """

    def transform(self, *args, **kwargs) -> Any:
        """ Returns values based on the trained model """
