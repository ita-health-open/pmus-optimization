from tensorflow.keras import models, layers, losses, optimizers, activations
from tensorflow.keras.layers import BatchNormalization, Conv1D, MaxPooling1D, LeakyReLU
from tensorflow.keras.callbacks import EarlyStopping
from .model import ModelInterface
from typing import Any, List
import numpy as np
from sklearn.base import BaseEstimator, RegressorMixin


class CNN1D(BaseEstimator, RegressorMixin):

    def __init__(self, num_samples: int, input_volume: int, output_size: int, filters: int = 5,
                 kernel_size: int = 5, pool_size: int = 2, n_cnn_layers: int = 9, alpha_leaky_relu: float = 0.1,
                 hidden_dense_layers=[200, 50], use_bias: bool = False, learning_rate: float = 1e-3,
                 decay: float = 1e-4, epochs=50) -> None:

        self.num_samples = num_samples
        self.input_volume = input_volume
        self.output_size = output_size
        self.filters = filters
        self.kernel_size = kernel_size
        self.pool_size = pool_size
        self.n_cnn_layers = n_cnn_layers
        self.alpha_leaky_relu = alpha_leaky_relu
        self.hidden_dense_layers = hidden_dense_layers
        self.use_bias = use_bias
        self.learning_rate = learning_rate
        self.decay = decay
        self.epochs = epochs

        self.model = models.Sequential()
        self.model.add(Conv1D(filters=filters, kernel_size=kernel_size, padding='same',
                       name=f'conv_1', use_bias=use_bias, input_shape=(num_samples, input_volume)))
        self.model.add(BatchNormalization(name='norm_1'))
        self.model.add(LeakyReLU(alpha=alpha_leaky_relu, name='leaky_relu_1'))
        self.model.add(MaxPooling1D(pool_size=pool_size, name='pool_1'))

        for n in range(1, n_cnn_layers-1):
            self.model.add(Conv1D(filters=filters, kernel_size=kernel_size,
                           padding='same', name=f'conv_{n+1}', use_bias=use_bias))
            self.model.add(BatchNormalization(name=f'norm_{n+1}'))
            self.model.add(LeakyReLU(alpha=alpha_leaky_relu,
                           name=f'leaky_relu_{n+1}'))
            self.model.add(MaxPooling1D(
                pool_size=pool_size, name=f'pool_{n+1}'))

        self.model.add(Conv1D(filters=filters, kernel_size=kernel_size,
                       padding='same', name=f'conv_{n_cnn_layers}', use_bias=use_bias))
        self.model.add(BatchNormalization(name=f'norm_{n_cnn_layers}'))
        self.model.add(LeakyReLU(alpha=alpha_leaky_relu,
                       name=f'leaky_relu_{n_cnn_layers}'))

        self.model.add(layers.Flatten())

        for n, units in enumerate(hidden_dense_layers):
            self.model.add(layers.Dense(
                units=units, activation=activations.linear, name=f'dense_{n+1}', use_bias=use_bias))
            self.model.add(BatchNormalization(name=f'norm_dense_{n+1}'))
            self.model.add(LeakyReLU(alpha=alpha_leaky_relu,
                           name=f'leaky_relu_dense_{n+1}'))

        self.model.add(layers.Dense(units=output_size, activation=activations.linear,
                       name=f'dense_{len(hidden_dense_layers)+1}', use_bias=use_bias))
        self.model.compile(
            optimizer=optimizers.Adam(
                learning_rate=learning_rate, decay=decay),
            loss=losses.mean_squared_error
        )

    def fit(self, X: List[np.array], y: np.array = None, validation_rate: float = 0.3):
        es_callback = EarlyStopping(
            monitor='val_loss', patience=20, restore_best_weights=True)
        ival = int(X.shape[0]*validation_rate)
        Xval, Xtrain, yval, ytrain = X[:ival], X[ival:], y[:ival], y[ival:]
        return self.model.fit(Xtrain, ytrain, epochs=self.epochs, verbose=1,
                              validation_data=(Xval, yval), callbacks=[es_callback])

    def transform(self, X: List[np.array]) -> Any:
        return self.model.predict(X)

    def fit_transform(self, X, y):
        self.fit(X, y)
        return self.transform(X)

    def predict(self, X):
        return self.model.predict(X)
